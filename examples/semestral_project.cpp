﻿#include "iimavlib/SDLDevice.h"
#include "iimavlib/Utils.h"
#include "iimavlib/WaveFile.h"
#include "iimavlib/AudioFilter.h"
#include "iimavlib_high_api.h"
#ifdef SYSTEM_LINUX
#include <unistd.h>
#endif
#include "iimavlib/video_ops.h"
#include <algorithm>

namespace iimavlib {

	class GraphicsHandler {
	public:
		GraphicsHandler(video_buffer_t& data) : data_(data) {}

		void two_slow_circles_animation();
		void one_fast_circle_animation();
		void three_regular_circles_animation();
		void four_circles_animation();
		void two_fast_circles_and_polygon_animation();
		void spiral_circles_animation();

	private:
		video_buffer_t& data_;
		double angle = 0.0;
		const double PI = 3.14159265358979323846;
	};

	// Function to create an animation with two slowly moving circles and a dynamic polygon.
	void GraphicsHandler::two_slow_circles_animation() {
		static double angle = 0.0;  // Static angle for continuity across function calls.

		// Position calculation for the first circle.
		// The circle oscillates horizontally around (400, 300) with a wider radius in the x-direction.
		rectangle_t position(400 - 64 + std::cos(angle) * 200,
			300 - 64 + std::sin(angle) * 150, 128, 128);
		// The color of the first circle alternates between green and red.
		draw_circle(data_, position, (static_cast<int>(angle * 100) % 2) ? rgb_t(0, 255, 0) : rgb_t(255, 0, 0));

		// Position calculation for the second circle.
		// This circle oscillates vertically around (400, 300) with a wider radius in the y-direction.
		rectangle_t position2(400 - 64 - std::sin(angle) * 150,
			300 - 64 - std::cos(angle) * 200, 128, 128);
		// The color of the second circle alternates between yellow and cyan.
		draw_circle(data_, position2, (static_cast<int>(angle * 100) % 2) ? rgb_t(255, 255, 0) : rgb_t(0, 255, 255));

		// Creating and drawing a dynamic polygon.
		std::vector<rectangle_t> points;
		int sides = 3 + (static_cast<int>(angle * 10) % 6); // The number of sides varies from 3 to 8.
		for (int i = 0; i < sides; ++i) {
			points.push_back(rectangle_t(400 + sin(angle + i * 6.28 / sides) * 200,
				300 + cos(i * 6.28 / sides) * 200));
		}
		draw_polygon(data_, points, sides % 2 ? rgb_t(0, 0, 255) : rgb_t(255, 0, 255));

		// Slowly increasing the angle for the next frame to create a slower animation.
		angle += 0.026;
	}


	// Function to create an animation with one fast-moving circle and a dynamic polygon.
	void GraphicsHandler::one_fast_circle_animation() {
		static double angle = 0.0; // Static angle to retain continuity in animation across function calls.

		// Position calculation for a single fast-moving circle.
		// The circle is centered around (400, 300) and moves in a reduced radius of 150 pixels.
		rectangle_t position(400 - 64 + std::cos(angle) * 150,
			300 - 64 + std::sin(angle) * 150, 128, 128);
		draw_circle(data_, position, rgb_t(0, 0, 255)); // Blue circle

		// Creating a vector to store points for the dynamic polygon.
		std::vector<rectangle_t> points;

		// Calculating the number of sides for the polygon based on the angle.
		// The polygon changes its shape as the angle increases.
		int sides = 3 + (static_cast<int>(angle * 10) % 6); // Sides vary between 3 and 8.

		// Loop to add points for the polygon, creating a dynamic shape.
		for (int i = 0; i < sides; ++i) {
			points.push_back(rectangle_t(400 + sin(angle + i * 6.28 / sides) * 200,
				300 + cos(angle + i * 6.28 / sides) * 200));
		}

		// Drawing the dynamically changing polygon.
		draw_polygon(data_, points, sides % 2 ? rgb_t(0, 0, 255) : rgb_t(255, 0, 255));

		// Incrementing the angle for a faster animation of the circle and polygon.
		// A larger increment (0.08) results in a faster movement and shape change.
		angle += 0.08;
	}

	// Function to create an animation with three circles moving regularly around a central point.
	void GraphicsHandler::three_regular_circles_animation() {
		static double angle = 0.0; // Static angle for smooth animation continuity between function calls.

		int radius = 150;

		// Position calculation for the first circle (Red).
		// The circle is centered around (400, 300) and moves in a radius of 150 pixels.
		rectangle_t position1(400 + std::cos(angle) * radius,
			300 + std::sin(angle) * radius, 128, 128);
		draw_circle(data_, position1, rgb_t(255, 0, 0)); // Red circle

		// Position calculation for the second circle (Green).
		// This circle is offset from the first by 2.0944 radians (or 120 degrees), creating a triangular formation.
		rectangle_t position2(400 + std::cos(angle + 2.0944) * radius,
			300 + std::sin(angle + 2.0944) * radius, 128, 128);
		draw_circle(data_, position2, rgb_t(0, 255, 0)); // Green circle

		// Position calculation for the third circle (Blue).
		// This circle is further offset by 4.18879 radians (or 240 degrees) relative to the first.
		rectangle_t position3(400 + std::cos(angle + 4.18879) * radius,
			300 + std::sin(angle + 4.18879) * radius, 128, 128);
		draw_circle(data_, position3, rgb_t(0, 0, 255)); // Blue circle

		// Drawing a polygon that changes the number of sides based on the angle.
		std::vector<rectangle_t> points;
		int sides = 3 + (static_cast<int>(angle * 10) % 6); // The number of sides varies from 3 to 8.
		for (int i = 0; i < sides; ++i) {
			points.push_back(rectangle_t(400 + sin(angle + i * 6.28 / sides) * 200,
				300 + cos(i * 6.28 / sides) * 200));
		}
		draw_polygon(data_, points, sides % 2 ? rgb_t(0, 0, 255) : rgb_t(255, 0, 255));

		// Increasing the angle for the next frame to animate the circles and polygon.
		// A moderate increment (0.04) is used for a balanced animation speed.
		angle += 0.04;
	}

	// Four circles moving in a square pattern
	void GraphicsHandler::four_circles_animation() {
		static double angle = 0.0; // Static angle to maintain continuity in animation.

		// Loop to create four circles.
		for (int i = 0; i < 4; ++i) {
			// Calculate the position for each circle.
			// The circles are arranged in a square pattern around the center (400, 300).
			// The angle offset (i * PI / 2) places each circle at a corner of the square.
			rectangle_t position(400 + 100 * std::cos(angle + i * PI / 2),
				300 + 100 * std::sin(angle + i * PI / 2), 128, 128);

			// Drawing each circle with a distinct color.
			// The color is determined by the iterator 'i', creating a unique color for each circle.
			draw_circle(data_, position, rgb_t(128 * (i % 2), 128 * ((i + 1) % 2), 128 * ((i + 2) % 2)));
		}

		// Incrementing the angle to animate the square movement in subsequent calls.
		// The increment value (0.02) is chosen for a moderate speed of animation.
		angle += 0.02;
	}

	// Two fast-moving circles with a fast-changing polygon
	// Function to create an animation with two fast-moving circles and a dynamically changing polygon.
	void GraphicsHandler::two_fast_circles_and_polygon_animation() {
		static double angle = 0.0; // Static angle for continuity in animation across function calls.

		// Position calculation for the first circle.
		// The circle is centered at (400, 300) and moves in a radius of 150 pixels.
		rectangle_t position1(400 + 150 * std::cos(angle),
			300 + 150 * std::sin(angle), 128, 128);

		// Position calculation for the second circle.
		// This circle is diametrically opposite to the first circle (angle offset by PI).
		rectangle_t position2(400 + 150 * std::cos(angle + PI),
			300 + 150 * std::sin(angle + PI), 128, 128);

		// Drawing the two circles.
		draw_circle(data_, position1, rgb_t(255, 0, 0)); // Red circle
		draw_circle(data_, position2, rgb_t(0, 255, 0)); // Green circle

		// Creating a vector to store the points of the polygon.
		std::vector<rectangle_t> points;

		// Loop to calculate and add points for the polygon.
		// The polygon has 5 sides and rotates based on the 'angle' variable.
		for (int i = 0; i < 5; ++i) {
			points.push_back(rectangle_t(400 + 100 * std::sin(angle + i * 2 * PI / 5),
				300 + 100 * std::cos(angle + i * 2 * PI / 5)));
		}

		// Drawing the polygon with dynamically changing points.
		draw_polygon(data_, points, rgb_t(0, 0, 255)); // Blue polygon

		// Incrementing the angle to animate the circles and the polygon in subsequent calls.
		// A larger increment (0.07) makes the animation faster.
		angle += 0.07;
	}


	// Function to create an animation of circles in a spiral pattern.
	void GraphicsHandler::spiral_circles_animation() {
		static double angle = 0.0; // Static angle to maintain continuity in animation across calls.

		// Loop to draw a series of circles forming a spiral.
		for (int i = 0; i < 10; ++i) {
			// Calculating the radius for each circle. It increases with each iteration to create a spiral.
			double rad = 20 * i; // 'i' acts as a step factor for the radius.

			// Calculating the position of each circle.
			// '400' and '300' are offsets for centering the spiral on the screen.
			// 'rad * cos(angle + i * PI / 5)' and 'rad * sin(angle + i * PI / 5)' determine the circular position.
			rectangle_t position(400 + rad * std::cos(angle + i * PI / 5),
				300 + rad * std::sin(angle + i * PI / 5), 128, 128);

			// Drawing each circle with a unique color.
			// The color is calculated based on the iterator 'i', creating a gradient effect.
			draw_circle(data_, position, rgb_t(25 * i, 255 - 25 * i, (25 * i) % 255));
		}

		// Incrementing the angle to animate the spiral in subsequent calls.
		// The smaller increment (0.01) ensures a smooth transition.
		angle += 0.01;
	}

	class Sound_Generator : public SDLDevice, public AudioFilter
	{
	public:
		static const rgb_t black;

		Sound_Generator(int width, int height)
			: SDLDevice(width, height, "Sound"),
			AudioFilter(pAudioFilter()),
			data_(rectangle_t(0, 0, width, height), black),
			index_(-1), position_(0), key_pressed(false),
			graphicsHandler_(data_) { // Initialize graphicsHandler_ after data_
			load_samples();  // Load sound samples
			start();  // Start the rendering thread
		}
		//Destructor
		~Sound_Generator() {
			// Stop the rendering thread
			stop();
		}
	private:
		std::vector<std::vector<audio_sample_t>> sounds_;  // Audio samples for the sounds
		video_buffer_t data_;  // Video data
		int index_;  // Index of currently playing sound
		size_t position_;  // Next sample to be played for current sound
		std::mutex position_mutex_;  // Mutex to lock index and position
		bool key_pressed;  // Flag for key press
		const double PI = 3.14159265358979323846;

		// Load sound samples
		void load_samples() {
			std::vector<std::string> filenames = {
				"../data/sound1.wav", "../data/sound2.wav", "../data/sound3.wav",
				"../data/sound4.wav", "../data/sound5.wav", "../data/sound6.wav"
			};
			for (const auto& filename : filenames) {
				if (!load_file(filename)) {
					throw std::runtime_error("Failed to load sounds samples!");
				}
			}
			logger[log_level::info] << "Sounds: " << sounds_.size();
		}


		// Key press handling
		//Return false if program has to stop
		bool do_key_pressed(const int key, bool pressed) {
			if (pressed) {
				switch (key) {
					// If key Q or ESCAPE was pressed, we want to exit the program
				case 'q':
				case keys::key_escape: return false;
					// Keys A, B, C, D, E and F should trigger sounds 0, 1, 2, 3, 4, and 5
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				{
					int idx = key - 'a'; // Index of current key (0 for a, 1 for b, 2 for c, 3 for d, 4 for e, 5 for f)
					logger[log_level::info] << "Sound " << idx;
					std::unique_lock<std::mutex> lock(position_mutex_); // Lock the variables
					index_ = idx;
					position_ = 0;
				} break;
				}
			}
			update_screen(); // Update the screen　iimediately to reflect the keypress
			return true;
		}

		// Update screen with animations
		void update_screen()
		{
			//Clear the screen
			rgb_t color = black;
			uint8_t intensity = 0;
			{
				std::unique_lock<std::mutex> lock(position_mutex_);
				if (index_ >= 0 && static_cast<size_t>(index_) < sounds_.size()) {
					intensity = static_cast<uint8_t>(255.0 - (255.0 * position_ / sounds_[index_].size()));
				}
			}

			data_.clear(color);

			// Graphic logic when some key is pressed
			if (key_pressed) {
				switch (index_) {
				case 0: graphicsHandler_.two_slow_circles_animation(); break;
				case 1: graphicsHandler_.one_fast_circle_animation(); break;
				case 2: graphicsHandler_.three_regular_circles_animation(); break;
				case 3: graphicsHandler_.four_circles_animation(); break;
				case 4: graphicsHandler_.two_fast_circles_and_polygon_animation(); break;
				case 5: graphicsHandler_.spiral_circles_animation(); break;

				default: break;
				}
			}

			logger[log_level::info] << ": " << key_pressed;


			// Push the updated buffer to the rendering thread
			blit(data_);
		}

		GraphicsHandler graphicsHandler_;

		// Process audio
		error_type_t do_process(audio_buffer_t& buffer) {
			// Return immediately if the process is stopped
			if (is_stopped()) return error_type_t::failed;

			// Get iterator to the start of the data buffer
			auto data = buffer.data.begin();

			// Check if there is no active sound or the index is out of range
			if (index_ < 0 || (sounds_.size() <= static_cast<size_t>(index_))) {
				// Fill the buffer with zeroes if no active sound
				std::fill(data, data + buffer.valid_samples, 0);
			}
			else {
				// Lock the position mutex to safely access shared variables
				std::unique_lock<std::mutex> lock(position_mutex_);

				// Reference to the current sound's buffer
				const auto& sound = sounds_[index_];
				size_t samples = sound.size(); // Total number of samples in the current sound
				size_t remaining = buffer.valid_samples; // Remaining samples to be processed
				size_t written = 0; // Number of samples written to the buffer

				// Check if there are non-copied samples left in the current sound
				if (position_ < samples) {
					// Calculate how many samples are available to be copied
					const size_t avail = samples - position_;
					// Determine the number of samples to write (whichever is smaller: available or remaining)
					written = (avail >= remaining) ? remaining : avail;

					// Define the range of samples to be copied
					auto first = sound.cbegin() + position_;
					auto last = (avail >= remaining) ? first + remaining : sound.cend();

					// Copy the samples to the buffer
					std::copy(first, last, data);

					// Update the position and reduce the remaining buffer size
					position_ += written;
					remaining -= written;
					key_pressed = true; // Indicate that a key is pressed
				}
				else {
					// All samples are copied, reset the current sound and key press status
					index_ = -1;
					position_ = 0;
					key_pressed = false;
				}

				// Fill the rest of the buffer with zeroes if any space left
				std::fill(data + written, data + written + remaining, 0);
			}

			// Update the display since the position might have changed
			update_screen();
			return error_type_t::ok;
		}

		// Load a wave file
		bool load_file(const std::string filename) {
			try {
				// Loading the wave file
				WaveFile wav(filename);
				logger[log_level::info] << "Loading file: " << filename;

				// Retrieving and checking audio parameters of the file
				const audio_params_t params = wav.get_params();
				if (params.rate != sampling_rate_t::rate_44kHz) {
					throw std::runtime_error("Wrong sampling rate. 44kHz expected.");
				}

				// Preparing the buffer to read data
				size_t samples = 44100;
				std::vector<audio_sample_t> data(samples);

				// Reading data from the wave file
				wav.read_data(data, samples);

				// Adjusting the size of the data buffer if necessary
				data.resize(samples);

				// Logging the number of samples read
				logger[log_level::info] << "Read " << samples << " samples from " << filename;

				// Storing the loaded data in the sounds vector
				sounds_.push_back(std::move(data));
			}
			catch (const std::exception& e) {
				// Handling exceptions during the file loading process
				logger[log_level::fatal] << "Failed to load " << filename << " (" << e.what() << ")";
				return false;
			}

			// Returning true indicates successful loading
			return true;
		}
	};
	const rgb_t Sound_Generator::black(0, 0, 0);

}


int main() {
	try {
		// Obtain the default audio device ID
		iimavlib::audio_id_t device_id = iimavlib::PlatformDevice::default_device();

		// Set up audio parameters
		iimavlib::audio_params_t params;
		params.rate = iimavlib::sampling_rate_t::rate_44kHz;

		// Create a filter chain with Sound_Generator and PlatformSink
		auto sink = iimavlib::filter_chain<iimavlib::Sound_Generator>(800, 600)
			.add<iimavlib::PlatformSink>(device_id)
			.sink();

		// Run the audio processing
		sink->run();
	}
	catch (const std::exception& e) {
		// Log any exceptions and terminate the application
		iimavlib::logger[iimavlib::log_level::fatal] << "The application ended unexpectedly with an error: " << e.what();
	}

	return 0;
}