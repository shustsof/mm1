# Sound player
---

## Project Activation

To activate and run the project, please follow these steps:

1. Run the BAT file named 'prepare_...'.
2. After executing, check the 'Build' folder.
3. Inside the 'Build' folder, launch the file named 'iimav.sln' through its release version.
4. This process will create a 'bin' folder within the 'Build' directory, and inside 'bin', a 'Release' folder will be generated.
5. Add the 'data' file from the project file to the 'bin' folder.
6. In the 'Release' folder, you need to add the 'sdl.dll' file, which can be found in the 'Build' folder.
7. Once all the files are in place, you can now run the semestral_project.exe file for the semester project.

Please ensure all steps are followed in order to correctly set up and run the project.

---

## Code explanation

This section provides a detailed explanation of the code for the "Sound player" project, highlighting its key functionalities and classes.

---

### Graphics Handling

#### Class: `GraphicsHandler`
- Manages various animations synchronized with audio playback.
- Each animation method, like `two_slow_circles_animation()`, creates unique visual patterns, ranging from moving circles to dynamic polygons.
- The `angle` and `PI` variables are used to calculate the positions and movements of graphical elements.

---

### Audio Processing

#### Class: `Sound_Generator`
- Extends the functionalities of `SDLDevice` and `AudioFilter`.
- Manages audio-visual synchronization, with each sound triggering a corresponding animation.
- Sound samples are loaded through `load_samples()` and played based on key presses handled in `do_key_pressed()`.
- `update_screen()` updates the graphical output, and `do_process()` is responsible for the audio processing logic.
---


### Application Flow

#### Function: `main()`
- Initializes the application, setting up the audio parameters and filter chain.
- Exception handling is implemented to manage any runtime errors gracefully.

---

### Detailed Function Descriptions

#### `GraphicsHandler::two_slow_circles_animation()`
- Animates two circles moving slowly, along with a dynamic polygon.
- Uses sine and cosine functions for smooth, oscillating movements.

#### `Sound_Generator::do_key_pressed()`
- Handles user input, with different keys triggering specific sounds and animations.
- Utilizes a mutex for thread-safe operations on shared resources.

#### `Sound_Generator::update_screen()`
- Redraws the screen with the current state of animations.
- Selects the appropriate animation based on the current sound being played.

#### `Sound_Generator::do_process()`
- Core method for audio processing.
- Fills the audio buffer with sound samples and adjusts the playback based on user interactions.

### Conclusion

The code for the "Sound player" project demonstrates a sophisticated integration of audio and graphics using C++ and the `iimavlib` library. The application offers an interactive experience by linking sounds with real-time animations, showcasing the capabilities of multimedia programming.

---
